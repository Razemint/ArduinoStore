<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="./css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/estilos.css">
    <title>Inicio de sesión</title>
  </head>
  <body>
    <h2>Hola mundo</h2>
    <div class="container">
      <form class="" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
        <div class="input-group">
          <i class="fa fa-user-o icons" -aria-hidden="false"></i>
          <input type="text" name="correo" placeholder="tu correo" class="form-control">
        </div>

        <div class="input-group">
          <i class="fa fa-lock icons" aria-hidden="false"></i>
          <input type="password" name="password" placeholder="tu contraseña" class="form-control">
        </div>

        <button type="submit" name="submit" class="btn btn-flat-green">Ingresar</button>
      </form>

   <a href="./registro.php"class="login-link">¿no tienes cuenta?</a>

    </div>
  </body>
</html>
