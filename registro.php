<!--inicio de php para comprobar la conexión entre la base de datos y el archivo php-->
<?php
  try {
     require_once './functions/bd.conexion.php';
  } catch (Exception $e) {
    $error = $e->getMessage();
  }

  require 'admin/config.php';

 ?>


<!DOCTYPE html>
<html lang="es">

<!-- Inicio de la cabecera-->
<head>
  <meta charset="utf-8">
  <meta name="description" content="Página dedicada a la venta de placas y componentes Arduino">
  <meta name="keywords" content="arduino, componentes, minipc, electrónica, placas">
  <title>Arduino Store: La mejor tienda de Arduino!</title>
  <link rel="stylesheet" type="text/css" href="./css/estilos.css">
</head>

<body>

      <header>
        <div id="cabecera"><a href="./index.php"><img src="./img/arduinostore.png"></a>
        </div>
      </header>



      <section class="contenedor">
        <h2>Registro de usuarios</h2>
        <form action="crear.php" id="registro "class="registro" action="index.html" method="post">
                <div id="datos_usuario" class="registro caja clearfix">
                  <div class="campo">
                    <label for="nombre">Nombre</label>
                    <input type="text" id="nombre" name="nombre" placeholder="tu nombre">
                  </div>

                  <div class="campo">
                    <label for="apellido">Apellido</label>
                    <input type="text" id="apellido" name="apellido" placeholder="tu apellido">
                  </div>

                  <div class="email">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" placeholder="tu email">
                  </div>

                  <div class="contraseña">
                    <label for="contraseña">Contraseña</label>
                    <input type="password" id="password" name="password" placeholder="tu contraseña">
                  </div>

                <br>
                <input type="submit" name="Enviar">

                </div>
              </div>
              <div id="error"></div>

            </div>
        </form>

      </section>
</body>
