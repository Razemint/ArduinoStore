/* PROMPTS Y ALERTS
var nombre = prompt('Cpmo te llamas?');
var apellido = prompt('como te apellidas?');
alert("Bienvenido " + nombre + " " + apellido);*/

/* DECLARANDO VARIABLES
var modelo = "emu";
var graficos = 100;
var potencia = 1000;
*/

/* OBJETOS
var clasesdarksouls = {
  clase: "guerrero",
  arma: "espadon",
  objeto: "humanidad",
  lvl: "5",
}

console.log (clasesdarksouls.arma)
*/

/* EDITANDO OBJETOS
var clasesdarksouls = {
  clase: "guerrero",
  arma: "espadon",
  objeto: "humanidad",
  lvl: "5",
}
console.log (clasesdarksouls.arma);
console.log (clasesdarksouls.clase);
console.log (clasesdarksouls.objeto);
console.log(clasesdarksouls.lvl);
*/

/*  arreglos/arrays
var radiohead = ["Pablo Honey", "The Bends", "OK Computer", "Kid A", "Amnesiac", "Hail to the Thief", "In/Rainbows", "The King of Limbs", "A Moon Shaped Pool"];

var nirvana = new Array("Bleach", "Nevermind", "In Utero");

para saber cuantas variables tiene el arreglo: radiohead.length
para poner el array en reversa: radiohead.reverse()
*/

/* MODIFICANDO ARRAYS
var radiohead = ["Pablo Honey", "The Bends", "OK Computer", "Kid A", "Amnesiac", "Hail to the Thief", "In/Rainbows", "The King of Limbs", "A Moon Shaped Pool"];
console.log(radiohead);

//para añadir un campo nuevo
radiohead.push("OKNOTOK 1997-2017");
console.log(radiohead);

//para reemplazar un campo
radiohead[2]=("Thom Yorke");
console.log(radiohead);

//para buscar un campo
 console.log(radiohead.indexOf("Kid A"));

//para quitar el último campo de un arreglo
radiohead.pop();
console.log(radiohead);

//para quitar un campo desde otro campo
radiohead.splice(1,5);
console.log("con splice "+ radiohead);

//Con filtro
console.log("filtro");
var radiohead = ["Pablo Honey", "The Bends", "OK Computer", "Kid A", "Amnesiac", "Hail to the Thief", "In/Rainbows", "The King of Limbs", "A Moon Shaped Pool"];
var nuevoArreglo = radiohead.filter(function(album) {
  return (album !== 'OK Computer');
});
console.log(nuevoArreglo);
*/

/* ESPACIOS EN BLANCO
var nombre = "Christian";
console.log(nombre);

var persona = {
      nombre: 'Christian',
      apellido: 'Martínez',
      profesion: 'Puto amo',
      bandaFavorita: 'radiohead';

}
console.log(persona);*/

/* NUMEROS
var numero1 = "12345";
var numero2 = 11;
var numero3 = 11.5;
var numero4 = "3.1416";
var numero5 = -11;
var numero6 = -11;

console.log(typeof(numero1));
console.log(typeof(numero2));
console.log(typeof(numero3));
console.log(typeof(numero4));
console.log(typeof(numero5));
console.log(typeof(numero6));

//convertir un string en un integer
var numero7 = parseInt(numero1);
console.log(numero7 + 5);

//convertir un string en un integer y sumarlo con decimales
var numero8 = parseFloat(numero4);
console.log(numero8 + 10);

//convertir un string en un integer y sumarlo con 2 decimales
var numero8 = parseFloat(numero4);
console.log(parseFloat(numero4).toFixed(2));
*/

/*FECHAS
//ver fecha completa
var fecha = new Date();
console.log(fecha);

//ver año
console.log("El año actual es: "+ fecha.getFullYear());

//ver fecha
console.log("el día actual es: "+ fecha.getDate());

//ver día de la semana
console.log("el día de la semana es: "+fecha.getDay());

//ver hora
console.log("la hora es: "+fecha.getHours());

//ver minutos
console.log("con: "+fecha.getMinutes());
*/

/* STRINGS
var texto = "esto es una cadena de texto";
console.log(texto);
//identificar que tipo es
console.log(typeof(texto));
//hacerlo en mayúscula
console.log(texto.toUpperCase());
//identificar cuantos caracteres tiene
console.log(texto.length);
//separarlo en palabras
console.log(texto.split(" "));
//encontrar la cadena donde se encuentra la palabra
console.log(texto.indexOf("cadena"));
//cortar una palabra de 0 a 11
console.log(texto.slice(0,11));
//hacerlo en minúscula
console.log(texto.toLowerCase());
*/

/*If-else if-else
var cantidadapagar = 100;
var saldo = 500;

if (saldo < cantidadapagar) {
  console.log("no hay saldo para pagar");
} else {
  console.log("el pago se realizó con éxito");
}

var edad1 = 20;
var edad2 = 20;

if (edad1 > edad2) {
  console.log("edad1 es mayor");
} else if (edad1 == edad2) {
  console.log("son las mismas");
}else {
  console.log("edad 2 es mayor");
}

var edad3 = "20";
var edad4 = 20;

if (edad1 == edad2) {
  console.log("son iguales");
} else {
  console.log("no son iguales");
}

var texto1 = "hola mundo";
var texto2 = "Hola Mundo";
if (texto1 == texto2) {
  console.log("son iguales");
} else {
  console.log("son diferentes");
}

if (texto1.toLowerCase() == texto2.toLowerCase()) {
  console.log("son iguales");
} else {
  console.log("son diferentes");
}
*/

/* if con && y ||
var cantidadapagar= 100;
var saldo = 101;
var recargaTelefono = 10;
var usuarioValido = true;

//&& significa y
if (saldo > cantidadapagar && usuarioValido) {
  console.log("se pagó correctamente");
} else {
  console.log("no se pudo pagar");
}

//|| significa o
var cantidadapagar = 1000;
var efectivo = 500;
var tarjetacredito = true;
if (efectivo > cantidadapagar || tarjetacredito) {
  console.log("pago realizado");
} else {
  console.log("te falta efectivo");
}
*/

/* +3 condiciones, usar switch
var metododepago = 'meme';
switch (metododepago) {
  case 'tc':
    console.log("se pagó con tarjeta de crédito");
    break;
    case 'efectivo':
    console.log("se pagó con efectivo");
      break;
      case 'paypal':
        console.log("se pagó con paypal");
        break;
  default:
  console.log("método no válido");
    break;
}*/

/*operadores
var numero1 = 10;
var numero2 = 20;
//sumar
console.log(numero1 + numero2);
//resta
console.log(numero1 - numero2);
//multiplicacion
console.log(numero1 * numero2);
//division
console.log(numero1 / numero2);

//incrementos
numero1--;
console.log(numero1);
numero1++;
console.log(numero1);

//sumar sobre la misma variable
var numero3 = 30;
console.log("el numero 3 "+ numero3);
numero3 += 20;
console.log("el numero 3 "+ numero3);
numero3 -= 20;
console.log("el numero 3 "+ numero3);
numero3 *= 20;
console.log("el numero 3 "+ numero3);
numero3 /= 20;
console.log("el numero 3 "+ numero3);

//ejemplo bergas
var cantidadapagar = 100;
var saldo = 101;
var recargaTelefono = 10;

if (saldo > cantidadapagar) {
  console.log("pago realizado");
  saldo -= cantidadapagar;
  console.log("tienes un saldo de "+ saldo);
  if (recargaTelefono < saldo) {
    console.log("la recarga se hizo con éxito");
    saldo -= recargaTelefono;
  } else {
    console.log("no alcanzó para la recarga, pendejo");
  }
} else {
  console.log("no se pudo pagar");
}
*/

/* Loops - FOR*/
