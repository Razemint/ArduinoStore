<!DOCTYPE html>
<html lang="es">

<!-- Inicio de la cabecera-->
<head>
  <meta charset="utf-8">
  <meta name="description" content="Página dedicada a la venta de placas y componentes Arduino">
  <meta name="keywords" content="arduino, componentes, minipc, electrónica, placas">
  <title>Arduino Store: La mejor tienda de Arduino!</title>
  <link rel="stylesheet" type="text/css" href="./css/estilos.css">

  <!--begin WOWSlider.com HEAD section-->
  <link rel="stylesheet" type="text/css" href="engine1/style.css" />
  <script type="text/javascript" src="engine1/jquery.js"></script>
  <!-- End WOWSlider.com HEAD section -->

</head>

<body>
  <header>
    <div id="cabecera"><a href="./index.php"><img src="./img/arduinostore.png"></a>
    </div>
  </header>

  <!--menú de navegación-->
  <ul id="menu-bar">
   <li class="active"><a href="#">Inicio</a></li>
   <li><a>Productos</a>
    <ul>
     <li><a href="./componentes.html">Componentes</a></li>
     <li><a href="./placas.html">Placas</a></li>
     <li><a href="./materiales.html">Materiales</a></li>
    </ul>
   </li>
   <li><a href="./ayudaysoporte.html">Ayuda y Soporte</a></li>
   <li><a href="./contacto.html">Contacto</a></li>
   <li><a href="./login.php">Iniciar Sesión</a></li>
   <li><a href="./registro.php">Registro</a></li>
  </ul>
  <!--fin del menú de navegación-->
<br>
<br>

  <!-- Start WOWSlider.com BODY section -->
  <div id="wowslider-container1">
  <div class="ws_images"><ul>
  		<li><img src="data1/images/arduinouno.jpg" alt="Arduino UNO" title="Arduino UNO" id="wows1_0"/>La mejor placa para comenzar a desarrollar en arduino</li>
  		<li><img src="data1/images/arduinomega.jpg" alt="Arduino MEGA" title="Arduino MEGA" id="wows1_1"/>Si quieres expandir tu desarrollo en arduino, con más conexiones</li>
  		<li><img src="data1/images/leds.jpg" alt="leds" title="leds" id="wows1_2"/>¿Necesitas LEDS? ¡Aquí tendrás los mejores!</li>
  		<li><img src="data1/images/arduinomicrocontrollers.jpg" alt="Placas Arduino" title="Placas Arduino" id="wows1_3"/>Una variedad de placas disponibles</li>
  		<li><img src="data1/images/sensorproximidad.jpg" alt="¡Nuevo!" title="¡Nuevo!" id="wows1_4"/>¡Ahora contamos con sensor de proximidad!</li>
  		<li><a href="http://wowslider.net"><img src="data1/images/cablesdepuentejumperwireparaarduinopicavrrobotd_nq_np_13040mlm20071284945_032014f.jpg" alt="bootstrap slider" title="Jumpers" id="wows1_5"/></a>Para enlazar todo lo que necesites</li>
  		<li><img src="data1/images/arduino_nano_v3_11000x750.jpg" alt="Arduino Nano" title="Arduino Nano" id="wows1_6"/>Si quieres desarrollar en un entorno pequeño, Nano es para ti</li>
  	</ul></div>
  	<div class="ws_bullets"><div>
  		<a href="#" title="Arduino UNO"><span><img src="data1/tooltips/arduinouno.jpg" alt="Arduino UNO"/>1</span></a>
  		<a href="#" title="Arduino MEGA"><span><img src="data1/tooltips/arduinomega.jpg" alt="Arduino MEGA"/>2</span></a>
  		<a href="#" title="leds"><span><img src="data1/tooltips/leds.jpg" alt="leds"/>3</span></a>
  		<a href="#" title="Placas Arduino"><span><img src="data1/tooltips/arduinomicrocontrollers.jpg" alt="Placas Arduino"/>4</span></a>
  		<a href="#" title="¡Nuevo!"><span><img src="data1/tooltips/sensorproximidad.jpg" alt="¡Nuevo!"/>5</span></a>
  		<a href="#" title="Jumpers"><span><img src="data1/tooltips/cablesdepuentejumperwireparaarduinopicavrrobotd_nq_np_13040mlm20071284945_032014f.jpg" alt="Jumpers"/>6</span></a>
  		<a href="#" title="Arduino Nano"><span><img src="data1/tooltips/arduino_nano_v3_11000x750.jpg" alt="Arduino Nano"/>7</span></a>
  	</div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">slider html</a> by WOWSlider.com v8.8</div>
  <div class="ws_shadow"></div>
  </div>
  <script type="text/javascript" src="engine1/wowslider.js"></script>
  <script type="text/javascript" src="engine1/script.js"></script>
  <!-- End WOWSlider.com BODY section -->
<br>
<br>
<section id="section">
<div class="Productos">
  <h2>Lo necesario para comenzar en Arduino:</h2>
  <ul class="nuestros_productos">
      <li>
        <img src="./img/arduinouno.png" alt="placa arduino uno">
        <p>Arduino UNO <span>$200</span></p>
        <a href="#" class="boton">Comprar</a>
      </li>

      <li>
        <img src="./img/Protoboard.png" alt="Protoboard">
        <p>Protoboard<span>$100</span></p>
        <a href="#" class="boton">Comprar</a>
      </li>
<br>
      <li>
        <img src="./img/jumpers.png" alt="jumpers">
        <p>Paquete de 20 jumpers<span>$50</span></p>
        <a href="#" class="boton">Comprar</a>
        <br>
      </li>

      <li>
        <img src="./img/resistencia.png" alt="resistencias">
        <p>Paquete de 20 resistencias <span>$30</span></p>
        <a href="#" class="boton">Comprar</a>
      </li>

      <li>
        <img src="./img/LEDS.png" alt="leds">
        <p>Paquete de 30 LEDS<span>$20</span></p>
        <a href="#" class="boton">Comprar</a>
      </li>

      <li>
        <img src="./img/matricial.png" alt="teclado matricial">
        <p>Teclado matricial<span>$50</span></p>
        <a href="#" class="boton">Comprar</a>
      </li>

  </ul>

</div>
</section>
<footer>

    s
</footer>

</body>
</html>
